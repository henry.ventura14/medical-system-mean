import { ToastsManager } from 'ng2-toastr';
import { ToastOptions } from 'ng2-toastr';
import { Injectable } from '@angular/core';

@Injectable()
export class ToastrService {
    constructor(public toastr: ToastsManager, public toastOPtions: ToastOptions) { }
    animate = 'flyRight'; // you can pass any options to override defaults
    dismiss = 'auto';
    toastLife = 10000;
    enableHTML: true;

    // Login
    typeSuccess() {
        this.toastr.success('Procesing', 'Ready!');
    }
    typeInfo() {
        this.toastr.info('Verify that the data is correct ',' Info!');
    }
    typeWarning() {
        this.toastr.warning('Password or incorrect email, if you fail 3 times the user will be blocked', 'Fail!');
    }
    typeDanger() {
        this.toastr.error('User blocked, contact an administrator ',' Error!');
    }

    // Register
    typeSuccessReg() {
        this.toastr.success('It was registered correctly ',' Ready!');
    }
    typeInfoReg() {
        this.toastr.info('Verify that all data is correct and do not forget the captcha ',' Info!');
    }
    typeWarningReg() {
        this.toastr.warning('Verify that what values correspond to each field ',' Fail!');
    }
    typeDangerReg() {
        this.toastr.error('There is already a user with the same document, user or email ',' Error!');
    }
    typeDangerPat() {
        this.toastr.error('Maybe there is a patient with the same document ',' Error!');
    }

    // Mailer
    typeInfoMail() {
        this.toastr.info('A message was sent to your email ',' Check');
    }
    typeDangerMail() {
        this.toastr.error('Verify that all data is correct and do not forget the captcha ',' Info!');
    }

    // ADMIN
    typeAdminDelUser() {
        this.toastr.info('A user was deleted', 'Info');
    }
    typeAdminEditUser() {
        this.toastr.info('Edited a user ',' Info');
    }
    typeAdminTypetUser() {
        this.toastr.info('The user type was changed', 'Info');
    }
    typeAdminUnlockUser() {
        this.toastr.info('A user user was unblocked', 'Info');
    }
    typeAdminBackup() {
        this.toastr.info('A backup of the database has been created', 'Info');
    }
    typeAdminRestore() {
        this.toastr.info('The database was restored', 'Info');
    }
    // Password
    typeChangePassSuccess() {
        this.toastr.info('Password changed', 'Info');
    }
    typeChangePassDanger() {
        this.toastr.error('Invalid pass', 'Error');
    }
    typeAdminSu(){
        this.toastr.info('The user type is changed now it is Admin', 'Info');
    }
    typeAdminMed(){
        this.toastr.info('The user type was changed now its Medical', 'Info');
    }
    typeForgotPassDanger(){
        this.toastr.error('Invalid answer', 'Error');
    }
    typeForgotEmailDanger(){
        this.toastr.error('Invalid email', 'Error');
    }


}
