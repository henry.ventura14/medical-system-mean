import { RouteInfo } from './sidebar.metadata';

export const ROUTES: RouteInfo[] = [
  {
    path: '/admin', title: 'Admin', icon: 'ft-shield', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
  },
  {
    path: '/dashboard', title: 'Start', icon: 'ft-layout', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
  },
  {
    path: '/patient', title: 'New Patient', icon: 'icon-book-open', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
  },
  {
    path: '/help', title: 'Help', icon: 'ft-life-buoy', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
  }

];
