import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

import { Patient } from '../../models/patients';
import { PatientService } from '../../services/patients.service';
import { UsersService } from '../../services/users.service';
import { AuthService } from '../../services/auth.service';
import { ToastrService } from '../toastr/toastr.service';
import { ReValidator } from '../validators/validators';
import { LoggerService } from 'app/services/logger.service';
import { MessageService } from 'app/services/email.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  providers: [PatientService]
})
export class NavbarComponent implements OnInit {
  @ViewChild('f') floatingLabelForm: NgForm;
  @ViewChild('vform') validationForm: FormGroup;
  @ViewChild('f') passwordsForm: NgForm;
  public searchText: string = "";
  public isCollapsed = true;
  public username: String = '';
  public modalReference: NgbModalRef;
  public closeResult: string;
  public isMed: Boolean;
  public _id: any;
  constructor(public patientService: PatientService, public users: UsersService, public _router: Router, public _user: AuthService, private modalService: NgbModal, private toastService: ToastrService, private loggerService: LoggerService, private _MessageService: MessageService) {
    this.getPatients();
  }
  formPass: FormGroup = new FormGroup({
    passwordA: new FormControl(null, [Validators.required, ReValidator(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,16}$/), Validators.minLength(8), Validators.maxLength(16)]),
    passwordB: new FormControl(null, [Validators.required]),
    passwordOld: new FormControl(null, [Validators.required]),
    action: new FormControl("Cambió de contraseña"),
    email: new FormControl(this.username = this._user.userLocal().email),
    user: new FormControl(this.username = this._user.userLocal().user)
  });
  ngOnInit() {
    if (!this._user.userLocal().med) {
      this.isMed = true;
    };
    this.username = this._user.userLocal().user;
  }
  changePass(form: NgForm) {
    this.users.changePassword(this._user.userLocal()._id, form.value)
      .subscribe(
        res => {
          console.log(res);
          this.onLogger(form.value);
          this.onMailForm(form.value);
          this.toastService.typeChangePassSuccess();
        },
        err => {
          console.log(err);
          this.toastService.typeChangePassDanger();
        })
  }

  open1(content1) {
    this.modalReference = this.modalService.open(content1, { centered: true });
    this.modalReference.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  Close() {
    this.modalReference.close();
  }

  getPatients() {
    this.patientService.getPatients()
      .subscribe(res => {
        this.patientService.patient = res as Patient[];
      })
  }

  logout() {
    this._user.logout()
      .subscribe(
        data => { console.log(data); this._router.navigate(['start']) },
        error => console.error(error)
      )
  }
  update() {
    this.getPatients();
  }
  onLogger(log) {
    this.loggerService.postLog(log).subscribe(res => {
      console.log("logger");
    })
  }
  onMailForm(mail) {
    this._MessageService.sendMessage(mail).subscribe(() => {
      this.toastService.typeInfoMail();
    });
  }
}
