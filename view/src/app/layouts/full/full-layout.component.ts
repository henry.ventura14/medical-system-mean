import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-full-layout',
    templateUrl: './full-layout.component.html',
    styleUrls: ['./full-layout.component.scss']
})

export class FullLayoutComponent implements OnInit {

    username: String = '';
    constructor(private _user: AuthService, private _router: Router) {
        this._user.user()
            .subscribe(
                data => this.addName(data),
                error => this._router.navigate(['start'])
            )
    }
    addName(data) {
        this.username = data.username;
    }

    ngOnInit() {
    }
}