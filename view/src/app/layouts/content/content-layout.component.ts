import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
@Component({
    selector: 'app-content-layout',
    templateUrl: './content-layout.component.html',
    styleUrls: ['./content-layout.component.scss']
})

export class ContentLayoutComponent implements OnInit {
    username: String = '';

    constructor(private _user: AuthService, private _router: Router) {
        this._user.user()
            .subscribe(
                data => this.addName(data),
                error => this._router.navigate(['start'])
            )
    }
    addName(data) {
        this.username = data.username;
    }
    ngOnInit() {
    }
}