export class Gas {

    constructor( diagGas = '', motiveGas='', examiningGas='', backgroundGas='', docName = '', doclastName = '',weight="", temp="",height="",ta="" ) {

        this.diagGas = diagGas;
        this.motiveGas = motiveGas;
        this.examiningGas = examiningGas;
        this.backgroundGas = backgroundGas;
        this.docName = docName;
        this.doclastName = doclastName;
        this.weight= weight;
        this.temp= temp;
        this.height= height;
        this.ta= ta;
    }

    diagGas: String;
    motiveGas: String;
    examiningGas: String;
    backgroundGas: String;
    docName: String;
    doclastName: String;
    weight: String;
    temp: String;
    height: String;
    ta: String;
}