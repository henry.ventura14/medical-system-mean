export class Record {
    constructor( der = [], gas = [], gen = [], gin = [], min = [], neu = [], onc = [], reu = [], tra = []) {
        
        this.der = der;
        this.gas = gas;
        this.gen = gen;
        this.gin = gin;
        this.min = min;
        this.neu = neu;
        this.onc = onc;
        this.reu = reu;
        this.tra = tra;
    }
    
    der: any = [];
    gas: any = [];
    gen: any = [];
    gin: any = [];
    min: any = [];
    neu: any = [];
    onc: any = [];
    reu: any = [];
    tra: any = [];
}
