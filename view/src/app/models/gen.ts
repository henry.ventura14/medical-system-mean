export class Gen {

    constructor( diagGen = '', motiveGen='', examiningGen='', backgroundGen='', docName = '', doclastName = '', weight="", temp="",height="",ta="") {

        this.diagGen = diagGen;
        this.motiveGen = motiveGen;
        this.backgroundGen = backgroundGen;
        this.examiningGen = examiningGen;
        this.docName = docName;
        this.doclastName = doclastName;
        this.weight= weight;
        this.temp= temp;
        this.height= height;
        this.ta= ta;

    }

    diagGen: String;
    backgroundGen: String;
    motiveGen: String;
    examiningGen: String;
    docName: String;
    doclastName: String;
    weight: String;
    temp: String;
    height: String;
    ta: String;

}