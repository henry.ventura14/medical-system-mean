export class Der {
    constructor(diagDer = '', motiveDer = '', backgroundDer = '', examiningDer = '', docName = '', doclastName = '', weight="", temp="",height="",ta="") {
        this.diagDer = diagDer;
        this.motiveDer = motiveDer;
        this.backgroundDer = backgroundDer;
        this.examiningDer = examiningDer;
        this.docName = docName;
        this.doclastName = doclastName;
        this.weight= weight;
        this.temp= temp;
        this.height= height;
        this.ta= ta;

    }
    diagDer: String;
    motiveDer: String;
    backgroundDer: String;
    examiningDer: String;
    docName: String;
    doclastName: String;
    weight: String;
    temp: String;
    height: String;
    ta: String;

}