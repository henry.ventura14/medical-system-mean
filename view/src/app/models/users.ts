export class User {
    constructor(_id = '', name = '', lastName = '', typeDocument = '', documentNumber = '', specialism = '', phone = '', direction = '', user = '', email = '', password = '', password2 = '', question = '', answer = '', su = false, med = true, block = true) {
        this._id = _id;
        this.name = name;
        this.lastName = lastName;
        this.typeDocument = typeDocument;
        this.documentNumber = documentNumber;
        this.specialism = specialism;
        this.phone = phone;
        this.direction = direction;
        this.user = user;
        this.email = email;
        this.password = password;
        this.password2 = password2;
        this.question = question;
        this.answer = answer;
        this.su = su;
        this.med = med;
        this.block = block;
    }
    _id: String;
    name: String;
    lastName: String;
    typeDocument: String;
    documentNumber: String;
    specialism: string;
    phone: String;
    direction: String;
    user: String;
    email: String;
    password: String;
    password2: String;
    question: String;
    answer: String;
    su: Boolean;
    med: Boolean;
    block: Boolean;
}
