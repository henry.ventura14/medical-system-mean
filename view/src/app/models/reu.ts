export class Reu {

    constructor(diagReu = '', motiveReu = '', examiningReu = '', backgroundReu = '', docName = '', doclastName = '', weight = "", temp = "", height = "", ta = "") {

        this.diagReu = diagReu;
        this.motiveReu = motiveReu;
        this.examiningReu = examiningReu;
        this.backgroundReu = backgroundReu;
        this.docName = docName;
        this.doclastName = doclastName;
        this.weight = weight;
        this.temp = temp;
        this.height = height;
        this.ta = ta;
        
    }

    diagReu: String;
    motiveReu: String;
    examiningReu: String;
    backgroundReu: String;
    docName: String;
    doclastName: String;
    weight: String;
    temp: String;
    height: String;
    ta: String;

}