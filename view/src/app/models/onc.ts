export class Onc {

    constructor(diagOnc = '', motiveOnc = '', examiningOnc = '', backgroundOnc = '', docName = '', doclastName = '', weight = "", temp = "", height = "", ta = "") {

        this.diagOnc = diagOnc;
        this.motiveOnc = motiveOnc;
        this.examiningOnc = examiningOnc;
        this.backgroundOnc = backgroundOnc;
        this.docName = docName;
        this.doclastName = doclastName;
        this.weight = weight;
        this.temp = temp;
        this.height = height;
        this.ta = ta;
    }

    diagOnc: String;
    motiveOnc: String;
    examiningOnc: String;
    backgroundOnc: String;
    docName: String;
    doclastName: String;
    weight: String;
    temp: String;
    height: String;
    ta: String;


}