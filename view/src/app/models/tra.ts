export class Tra {

    constructor(diagTra = '', motiveTra = '', examiningTra = '', backgroundTra = '', docName = '', doclastName = '', weight = "", temp = "", height = "", ta = "") {
        this.diagTra = diagTra;
        this.motiveTra = motiveTra;
        this.examiningTra = examiningTra;
        this.backgroundTra = backgroundTra;
        this.docName = docName;
        this.doclastName = doclastName;
        this.weight = weight;
        this.temp = temp;
        this.height = height;
        this.ta = ta;
    }
    diagTra: String;
    motiveTra: String;
    examiningTra: String;
    backgroundTra: String;
    docName: String;
    doclastName: String;
    weight: String;
    temp: String;
    height: String;
    ta: String;
}