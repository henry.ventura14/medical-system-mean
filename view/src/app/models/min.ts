export class Min {

    constructor(diagMin = '', motiveMin = '', examiningMin = '', backgroundMin = '', docName = '', doclastName = '', weight = "", temp = "", height = "", ta = "") {

        this.diagMin = diagMin;
        this.motiveMin = motiveMin;
        this.examiningMin = examiningMin;
        this.backgroundMin = backgroundMin;
        this.docName = docName;
        this.doclastName = doclastName;
        this.weight = weight;
        this.temp = temp;
        this.height = height;
        this.ta = ta;
    }

    diagMin: String;
    motiveMin: String;
    examiningMin: String;
    backgroundMin: String;
    docName: String;
    doclastName: String;
    weight: String;
    temp: String;
    height: String;
    ta: String;


}