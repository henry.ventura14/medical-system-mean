export class Log {
    constructor(user = '', email = '', ip = '', action = '') {
        this.user = user;
        this.email = email;
        this.ip = ip;
        this.action = action;
    }
    user: String;
    email: String;
    ip: String;
    action: String;
}