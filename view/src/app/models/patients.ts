export class Patient {

    constructor(_id = '', name = '', lastName = '', typeDocument = '', documentNumber = '', birth = '', phone = '', direction = '', gender = '', nameAffiliate = '', nameAgent = '', plan = '') {

        this._id = _id;
        this.name = name;
        this.lastName = lastName;
        this.typeDocument = typeDocument;
        this.documentNumber = documentNumber;
        this.birth = birth;
        this.gender = gender;
        this.phone = phone;
        this.nameAffiliate = nameAffiliate;
        this.nameAgent = nameAgent;
        this.direction = direction;
        this.plan = plan;
    }

    _id: String;
    name: String;
    lastName: String;
    typeDocument: String;
    documentNumber: String;
    birth: String;
    gender: String;
    phone: String;   
    nameAffiliate: String;
    nameAgent: String;
    direction: String;
    plan: String;
}
