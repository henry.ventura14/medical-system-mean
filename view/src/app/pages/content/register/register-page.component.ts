import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm, FormControl, FormGroup, Validators, NgModel } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from '../../../shared/toastr/toastr.service';
import { CustomValidators } from 'ng2-validation';
import { ReValidator } from '../../../shared/validators/validators';
import { User } from '../../../models/users';
import { UsersService } from '../../../services/users.service';
import { MessageService } from '../../../services/email.service';
import { LoggerService } from '../../../services/logger.service';
@Component({
    selector: 'app-register-page',
    templateUrl: './register-page.component.html',
    styleUrls: ['./register-page.component.scss'],
    providers: [UsersService, ToastrService]
})
export class RegisterPageComponent implements OnInit {
    constructor(private usersService: UsersService, private toastrSerivice: ToastrService, private router: Router, public _MessageService: MessageService, private loggerService: LoggerService) { }
    @ViewChild('f') floatingLabelForm: NgForm;
    @ViewChild('vform') validationForm: FormGroup;
    @ViewChild('f') registerForm: NgForm;
    public regularForm: FormGroup;
    public gender: any;
    public typeDocument: any;
    public specialism: any;
    public doc = true;
    public med: any;
    public password1: any;
    public passwors2: any;
    passwordCheck;

    ngOnInit() {
        this.med = {
            left: false
        }
        this.gender = [
            {
                "gender": "Masculino"
            },
            {
                "gender": "Femenino"
            }
        ];
        this.typeDocument = [
            {
                "type": "V"
            },
            {
                "type": "E"
            },
            {
                "type": "P"
            }
        ];
        this.specialism = [
            {
                "spec": "Dermatology"
            },
            {
                "spec": "Gastroenrology"
            },
            {
                "spec": "General"
            },
            /* {
                  "spec": "Ginecologia"
              },
              */
            {
                "spec": "Internal-Medicine"
            },
            /*    {
                    "spec": "Neumonologia"
                },*/
            {
                "spec": "Oncology"
            },
            {
                "spec": "Rheumatology"
            },
            {
                "spec": "Traumatology"
            }
        ];
        this.regularForm = new FormGroup({
            name: new FormControl(null, [Validators.required, ReValidator(/^[a-zA-Z- ]*$/)]),
            lastName: new FormControl(null, [Validators.required, ReValidator(/^[a-zA-Z- ]*$/)]),
            typeDocument: new FormControl(null, [Validators.required]),
            documentNumber: new FormControl(null, [Validators.required, CustomValidators.number]),
            specialism: new FormControl(null, [Validators.required]),
            phone: new FormControl(null, [Validators.required, CustomValidators.number]),
            direction: new FormControl(null, [Validators.required]),
            user: new FormControl(null, [Validators.required]),
            email: new FormControl(null, [Validators.required, Validators.email]),
            passwordA: new FormControl(null, [Validators.required, ReValidator(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,16}$/), Validators.minLength(8), Validators.maxLength(16)]),
            passwordB: new FormControl(null, [Validators.required]),
            question: new FormControl(null, [Validators.required]),
            answer: new FormControl(null, [Validators.required]),
            su: new FormControl(false),
            med: new FormControl(true),
            block: new FormControl(true),
            action: new FormControl('User register'),
            captcha: new FormControl(null, Validators.required)
        }, { updateOn: 'blur' });
    }
    resetForm(form?: NgForm) {
        if (form) {
            form.reset();
            this.usersService.selectedUser = new User();
        }
    }
    addUser(form: NgForm) {
        if (!this.regularForm.valid) {
            this.toastrSerivice.typeWarningReg();
            return;
        }
        this.usersService.postUser(form.value)
            .subscribe(
                res => {
                    this.loggerSession(form.value);
                    this.mailForm(form.value);
                    this.toastrSerivice.typeSuccessReg();
                    this.resetForm(form);
                    this.router.navigate(['start']);
                },
                err => {
                    this.toastrSerivice.typeDangerReg();
                    return;
                })
    }
    mailForm(form) {
        this._MessageService.sendMessage(form).subscribe(() => {
            this.toastrSerivice.typeInfoMail();
        });
    }
    loggerSession(log) {
        this.loggerService.postLog(log).subscribe(res => {
            console.log("logger");
        })
    }
}