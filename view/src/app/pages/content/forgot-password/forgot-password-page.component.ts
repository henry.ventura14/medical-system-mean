import { Component } from '@angular/core';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from '../../../services/email.service';
import { LoggerService } from '../../../services/logger.service';
import { ToastrService } from '../../../shared/toastr/toastr.service';
import { UsersService } from '../../../services/users.service';
import { ReValidator } from 'app/shared/validators/validators';

@Component({
    selector: 'app-forgot-password-page',
    templateUrl: './forgot-password-page.component.html',
    styleUrls: ['./forgot-password-page.component.scss']
})
export class ForgotPasswordPageComponent {
    id;
    mail;
    _user;
    public user: any;
    public ask: any
    public email: boolean;
    forgotForm: FormGroup = new FormGroup({
        email: new FormControl(null, [Validators.email, Validators.required])
    });
    forgotFormPass: FormGroup = new FormGroup({
        passwordA: new FormControl(null, [Validators.required, ReValidator(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,16}$/), Validators.minLength(8), Validators.maxLength(16)]),
        passwordB: new FormControl(null, [Validators.required]),
        answer: new FormControl(null, [Validators.required]),
        action: new FormControl("Pregunta secreta"),
        email: new FormControl(this.mail),
        captcha: new FormControl(null, Validators.required),
        user: new FormControl(this.user)
    });
    constructor(
        private route: Router, private loggerService: LoggerService, private usersService: UsersService, private toasterService: ToastrService, private _MessageService: MessageService) { }
    onEmail(form: NgForm) {
        this.usersService.email(form.value)
            .subscribe(res => {
                this.user = res;
                this.ask = this.user.question;
                this.email = true;
                this.id = this.user._id;
                this._user = this.user.user;
                this.mail = this.user.email;
            },
                err => {
                    this.toasterService.typeForgotEmailDanger();
                    return;
                });
    }
    onForgot(form: NgForm) {
        this.usersService.forgotPassword(this.id, form.value)
            .subscribe(
                res => {
                    this.onLogger(form.value);
                    this.onMailForm(form.value);
                    this.toasterService.typeChangePassSuccess();
                    this.route.navigate(['start']);
                },
                err => {
                    this.toasterService.typeForgotPassDanger();
                    return;
                }
            )
    }
    onLogger(log) {
        this.loggerService.postLog(log).subscribe(res => {
            console.log("logger");
        })
    }
    onMailForm(mail) {
        this._MessageService.sendMessage(mail).subscribe(() => {
            this.toasterService.typeInfoMail();
        });
    }
}

