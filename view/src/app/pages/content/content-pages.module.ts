import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CustomFormsModule } from 'ng2-validation';
import { MatchHeightModule } from "../../shared/directives/match-height.directive";
import { ContentPagesRoutingModule } from "./content-pages-routing.module";

import { ErrorPageComponent } from "./error/error-page.component";
import { ForgotPasswordPageComponent } from "./forgot-password/forgot-password-page.component";
import { LoginPageComponent } from "./login/login-page.component";
import { RegisterPageComponent } from "./register/register-page.component";

import { RecaptchaFormsModule } from 'ng-recaptcha/forms';
import {
    RECAPTCHA_LANGUAGE,
    RecaptchaModule,
} from 'ng-recaptcha';

import { CheckPasswordDirect } from 'app/shared/directives/check.pass.directive';

@NgModule({
    imports: [
        CommonModule,
        ContentPagesRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        MatchHeightModule,
        CustomFormsModule,
        NgbModule,
        RecaptchaFormsModule,
        RecaptchaModule.forRoot()
    ],
    declarations: [
        ErrorPageComponent,
        ForgotPasswordPageComponent,
        LoginPageComponent,
        RegisterPageComponent,
        CheckPasswordDirect
    ],
    providers: [
        {
            provide: RECAPTCHA_LANGUAGE,
            useValue: 'en'
        }
    ]
})
export class ContentPagesModule { }
