import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { ToastrService } from '../../../shared/toastr/toastr.service';
import { LoggerService } from '../../../services/logger.service';
@Component({
    selector: 'app-login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
    user: any;
    loginForm: FormGroup = new FormGroup({
        email: new FormControl(null, [Validators.email, Validators.required]),
        password: new FormControl(null, Validators.required),
        action: new FormControl("Login"),
        user: new FormControl("Anonimus"),
        captcha: new FormControl(null, Validators.required)
    });
    constructor(private _router: Router, private authService: AuthService, private toastrSerivice: ToastrService, private loggerService: LoggerService) { }

    ngOnInit() {
    }
    logout() {
        this.authService.logout()
            .subscribe(
                data => { console.log(data); this._router.navigate(['start']) },
                error => console.error(error)
            )
    }
    moveToRegister() {
        this._router.navigate(['register']);
    }

    login() {
        if (!this.loginForm.valid) {
            this.toastrSerivice.typeInfo();
            return;
        }
        this.authService.login(JSON.stringify(this.loginForm.value))
            .subscribe(
                data => {
                    this.user = data;
                    if (this.user.block) {
                        this.toastrSerivice.typeDanger();
                        this.logout();
                        this.loginForm.reset();
                        return;
                    } if (!this.user.block) {
                        localStorage.setItem('User', JSON.stringify(this.user.user));
                        this.loggerSession(this.loginForm.value);
                        this._router.navigate(['dashboard']);
                    }
                },
                error => {
                    this.loginForm.reset();
                    this.user = error;
                    if (this.user.error.block) {
                        this.toastrSerivice.typeDanger();
                        return;
                    } else {
                        this.toastrSerivice.typeWarning();
                        return;
                    }
                }
            )
    }
    loggerSession(log) {
        this.loggerService.postLog(log).subscribe(res => {
        })
    }
}