import { Component, OnInit } from '@angular/core';
import { PatientService } from 'app/services/patients.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private patientService: PatientService) {

  }

  ngOnInit() {
  }

}
