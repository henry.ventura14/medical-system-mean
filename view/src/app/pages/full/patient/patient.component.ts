import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { ReValidator } from '../../../shared/validators/validators';
import { Patient } from '../../../models/patients';
import { ToastrService } from '../../../shared/toastr/toastr.service';
import { PatientService } from '../../../services/patients.service';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
@Component({
    selector: 'app-patient',
    templateUrl: './patient.component.html',
    styleUrls: ['./patient.component.scss'],
    providers: [PatientService, ToastrService]
})
export class PatientComponent implements OnInit {
    constructor(private patientService: PatientService, private toastrSerivice: ToastrService, private authService: AuthService, private router: Router) {
    }
    @ViewChild('f') floatingLabelForm: NgForm;
    @ViewChild('vform') validationForm: FormGroup;
    @ViewChild('f') registerForm: NgForm;
    regularForm: FormGroup;
    public isSu: Boolean;
    public gender: any;
    public typeDocument: any;
    ngOnInit() {
        if (this.authService.userLocal().su) {
            this.isSu = true;
            this.router.navigate(['dashboard']);

        };
        this.gender = [
            {
                "gender": "Male"
            },
            {
                "gender": "Female"
            }
        ];

        this.typeDocument = [
            {
                "type": "V"
            },
            {
                "type": "E"
            },
            {
                "type": "P"
            }
        ];
        this.regularForm = new FormGroup({
            'phone': new FormControl(null, [Validators.required, CustomValidators.number]),
            'documentNumber': new FormControl(null, [Validators.required, CustomValidators.number]),
            'typeDocument': new FormControl(null, [Validators.required]),
            'gender': new FormControl(null, [Validators.required]),
            'name': new FormControl(null, [Validators.required, ReValidator(/^[a-zA-Z- ]*$/)]),
            'nameAgent': new FormControl(null, [ReValidator(/^[a-zA-Z- ]*$/)]),
            'nameAffiliate': new FormControl(null, [ReValidator(/^[a-zA-Z- ]*$/)]),
            'lastName': new FormControl(null, [Validators.required, ReValidator(/^[a-zA-Z- ]*$/)]),
            'direction': new FormControl(null, [Validators.required]),
            'birth': new FormControl(null, [Validators.required]),
            'plan': new FormControl(null)
        }, { updateOn: 'blur' });
    }
    resetForm(form?: NgForm) {
        if (form) {
            form.reset();
            this.patientService.selectedPatient = new Patient();
        }
    }
    addPatient(form: NgForm) {
        if (!this.regularForm.valid) {
            this.toastrSerivice.typeDangerPat();
            return;
        }
        this.patientService.postPatient(form.value)
            .subscribe(
                res => {
                    this.getPatients();
                    this.toastrSerivice.typeSuccess();
                    this.resetForm(form);
                    this.router.navigate(['dashboard']);

                }, err => {
                    this.toastrSerivice.typeDangerPat();
                })
    }
    getPatients() {
        this.patientService.getPatients()
            .subscribe(res => {
                this.patientService.patient = res as Patient[];
            })
    }
}