import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HelpComponent } from './help/help.component';
import { PatientComponent } from './patient/patient.component';
import { ReportsComponent } from './reports/reports.component';
import { DetailPatientComponent } from './detail-patient/detail-patient.component';
import { ConsultationComponent } from './consultation/consultation.component';
import { AdminComponent } from "./admin/admin.component";

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'admin',
        component: AdminComponent,
        data: {
          title: 'Admin'
        }
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
        data: {
          title: 'Dashboard'
        }
      },
      {
        path: 'help',
        component: HelpComponent,
        data: {
          title: 'Help'
        }
      },
      {
        path: 'consultation/:id',
        component: ConsultationComponent,
        data: {
          title: 'Consultation'
        }
      },
      {
        path: 'patient',
        component: PatientComponent,
        data: {
          title: 'Patient'
        }
      },
      {
        path: 'info-patient/:id',
        component: DetailPatientComponent,
        data: {
          title: 'Patient'
        }
      },
      {
        path: 'reports',
        component: ReportsComponent,
        data: {
          title: 'Reports'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FullPagesRoutingModule { }
