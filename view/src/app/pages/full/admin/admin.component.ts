import { Component, OnInit, Input, ViewChild, style } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from '../../../services/users.service';
import { AuthService } from '../../../services/auth.service';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { ReValidator } from '../../../shared/validators/validators';
import { ToastrService } from '../../../shared/toastr/toastr.service';
import { User } from '../../../models/users';
import { LoggerService } from '../../../services/logger.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
  providers: [UsersService]
})
export class AdminComponent implements OnInit {
  @ViewChild('f') floatingLabelForm: NgForm;
  @ViewChild('vform') validationForm: FormGroup;
  @ViewChild('f') registerForm: NgForm;
  public searchUser: string = "";
  public searchLog: string = "";
  public specialism: any;
  public typeDocument: any;
  public gender: any;
  public table = true;
  public log = true;
  public backup = true;
  public restore = true;
  public edit = true;
  public user: any;
  public logger: any;
  public infoUser: any;
  public regularForm: FormGroup;
  public _id: any;
  public backupForm: FormGroup;
  public restoreForm: FormGroup;
  public page;
  constructor(public usersService: UsersService, public authService: AuthService, private _router: Router, public toastrSerivice: ToastrService, private loggerService: LoggerService) {

  }
  formatDate(datetime) {
    let formated_datetime = new Date(datetime);
    return formated_datetime.getDate() + '-' + (formated_datetime.getMonth() + 1) + '-' + formated_datetime.getFullYear() + ' | ' + formated_datetime.getHours() + ' : ' + formated_datetime.getMinutes();
  }
  updateList() {
    this.usersService.getUsers();
  }
  ngOnInit() {
    if (!this.authService.userLocal().su) {
      this._router.navigate(['dashboard']);
    }
    this.usersService.getUsers()
      .subscribe(res => {
        this.user = res;
      });
    this.loggerService.getLogs()
      .subscribe(res => {
        this.logger = res;
      });

    this.specialism = [
      {
        "spec": "Cardiology"
      },

      {
        "spec": "Gastroentology"
      },
      {
        "spec": "General"
      },
      {
        "spec": "Gynecology"
      },
      {
        "spec": "Internal"
      },
      {
        "spec": "Neumonologia"
      },
      {
        "spec": "Pediatry"
      },
      {
        "spec": "Rheumatology"
      },
      {
        "spec": "Traumatology"
      }
    ];
    this.typeDocument = [
      {
        "type": "V"
      },
      {
        "type": "E"
      },
      {
        "type": "P"
      }
    ];

    this.regularForm = new FormGroup({
      '_id': new FormControl(null),
      'name': new FormControl(null, [Validators.required, ReValidator(/^[a-zA-Z- ]*$/)]),
      'lastName': new FormControl(null, [Validators.required, ReValidator(/^[a-zA-Z- ]*$/)]),
      'typeDocument': new FormControl(null, [Validators.required]),
      'documentNumber': new FormControl(null, [Validators.required, CustomValidators.number]),
      'phone': new FormControl(null, [Validators.required, CustomValidators.number]),
      'direction': new FormControl(null, [Validators.required]),
      'user': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(16)]),
      'email': new FormControl(null, [Validators.required, Validators.email])
    }, { updateOn: 'blur' });

    this.backupForm = new FormGroup({
      'action': new FormControl("Backup database"),
      'user': new FormControl(this.authService.userLocal().user),
      'email': new FormControl(this.authService.userLocal().email)
    }, { updateOn: 'blur' });

    this.restoreForm = new FormGroup({
      'nameRestore': new FormControl(null),
      'action': new FormControl("Restore database"),
      'user': new FormControl(this.authService.userLocal().user),
      'email': new FormControl(this.authService.userLocal().email)
    }, { updateOn: 'blur' });

  }
  resetForm(form?: NgForm) {
    if (form) {
      form.reset();
      this.usersService.selectedUser = new User();
    }
  }
  getUser(_id: string) {
    this.usersService.getUser(_id)
      .subscribe((res) => {
        this.infoUser = res;
      })
  }
  editUser(form: NgForm) {
    this.usersService.putUser(form.value._id, form.value)
      .subscribe(
        res => {
          this.updateList();
          this.toastrSerivice.typeSuccessReg();
        },
        err => {
          console.error(err)
          this.toastrSerivice.typeDangerReg();
          return;
        })
  }
  deltUser(_id: string) {
    if (confirm('¿Be sure you want to delete the user?')) {
      this.usersService.deleteUser(_id)
        .subscribe((res) => {
          this.updateList();
          this.toastrSerivice.typeAdminDelUser();
        })
    }
  }
  unlockUsers(_id: string) {
    if (confirm('¿Desea desbloquear el usuario?')) {
      this.usersService.unlockUser(_id).subscribe(res => {
        this.toastrSerivice.typeAdminUnlockUser();
      })
    }
  }
  backupSave(form: NgForm) {
    this.usersService.backup(form.value)
      .subscribe(res => {
        this.loggerSession(form.value);
        this.toastrSerivice.typeAdminBackup();
      })
  }
  restoreDb(form: NgForm) {
    this.usersService.restore(form.value)
      .subscribe(res => {
        this.loggerSession(form.value);
        this.toastrSerivice.typeAdminRestore();
      })
  }
  typeMed(id) {
    if (confirm('¿Desea cambiar el tipo de usuario de Admin a Medico?')) {
    this.usersService.typeUserMed(id).subscribe(
      res => {
        this.toastrSerivice.typeAdminMed();
      });
    }
  }
  typeSu(id) {
    if (confirm('¿Desea cambiar el typo de usuario de Medico aAdmin?')) {
    this.usersService.typeUserSu(id).subscribe(
      res => {
        this.toastrSerivice.typeAdminSu();
      });
    }
  }
  loggerSession(log) {
    this.loggerService.postLog(log).subscribe(res => {
      console.log("logger");
    })
  }
}

