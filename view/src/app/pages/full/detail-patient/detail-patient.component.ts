import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { ReValidator } from '../../../shared/validators/validators';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from '../../../shared/toastr/toastr.service';
import { AuthService } from '../../../services/auth.service';
import { PatientService } from '../../../services/patients.service';
import { RecordService } from '../../../services/record.service';
import { Patient } from '../../../models/patients';
@Component({
  selector: 'app-detail-patient',
  templateUrl: './detail-patient.component.html',
  styleUrls: ['./detail-patient.component.scss'],
  providers: [PatientService, RecordService]
})
export class DetailPatientComponent implements OnInit {
  regularForm: FormGroup;
  @ViewChild('f') floatingLabelForm: NgForm;
  @ViewChild('vform') validationForm: FormGroup;
  @ViewChild('f') registerForm: NgForm;
  public isCollapsed = true;
  public gender: any;
  public typeDocument: any;
  info: any;
  hist: any;
  der: any;
  gas: any;
  gen: any;
  gin: any;
  min: any;
  neu: any;
  onc: any;
  reu: any;
  tra: any;
  isSu: boolean;
  searchDateConsult;
  searchConsult;
  constructor(private recordService: RecordService, private toastrSerivice: ToastrService, private patientService: PatientService, private route: ActivatedRoute, private router: Router, private authService: AuthService) {
    this.route.params.subscribe(data => {
      this.patientService.getPatient(data.id)
        .subscribe((res) => {
          this.info = res;
          localStorage.setItem('Patient', JSON.stringify(this.info));
        })
    })
    this.route.params.subscribe(data => {
      this.recordService.getRecord(data.id)
        .subscribe(res => {
          this.hist = res;
          this.der = this.hist.der;
          this.gas = this.hist.gas;
          this.gen = this.hist.gen;
          this.gin = this.hist.gin;
          this.min = this.hist.min;
          this.neu = this.hist.neu;
          this.onc = this.hist.onc;
          this.reu = this.hist.reu;
          this.tra = this.hist.tra;
        })
    })
  }

  formatDate(datetime) {
    let formated_datetime = new Date(datetime);
    return formated_datetime.getDate() + '-' + (formated_datetime.getMonth() + 1) + '-' + formated_datetime.getFullYear() + ' | ' + formated_datetime.getHours() + ' : ' + formated_datetime.getMinutes();
  }

  createRecord(_id) {
    this.recordService.postRecord(_id)
      .subscribe(() => {
      })
  }
  editPatient(form: NgForm) {
    this.route.params.subscribe(data => {
      this.patientService.putPatient(data.id, form.value)
        .subscribe(() => {
          this.patientService.getPatient(data.id);
          this.toastrSerivice.typeSuccess();
        })
    })
  }
  ngOnInit() {
    if (this.authService.userLocal().su) {
      this.isSu = true;
      this.router.navigate(['dashboard']);

    };
    this.gender = [
      {
        "gender": "Masculino"
      },
      {
        "gender": "Femenino"
      }
    ];
    this.typeDocument = [
      {
        "type": "V"
      },
      {
        "type": "E"
      },
      {
        "type": "P"
      }
    ];
    this.regularForm = new FormGroup({
      'phone': new FormControl(null, [Validators.required, CustomValidators.number]),
      'documentNumber': new FormControl(null, [Validators.required, CustomValidators.number]),
      'typeDocument': new FormControl(null, [Validators.required]),
      'gender': new FormControl(null, [Validators.required]),
      'name': new FormControl(null, [Validators.required, ReValidator(/^[a-zA-Z- ]*$/)]),
      'lastName': new FormControl(null, [Validators.required, ReValidator(/^[a-zA-Z- ]*$/)]),
      'direction': new FormControl(null, [Validators.required]),
      'birth': new FormControl(null, [Validators.required]),
      'plan': new FormControl(null, [Validators.required])
    }, { updateOn: 'change' });
  }
  resetForm(form?: NgForm) {
    if (form) {
      form.reset();
      this.patientService.selectedPatient = new Patient();
    }
  }
}

