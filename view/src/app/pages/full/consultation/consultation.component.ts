import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { ToastrService } from '../../../shared/toastr/toastr.service';
import { RecordService } from '../../../services/record.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PatientService } from '../../../services/patients.service';
import { UsersService } from '../../../services/users.service';
import { AuthService } from '../../../services/auth.service';
import { Der } from '../../../models/der';
import { Gas } from '../../../models/gas';
import { Gen } from '../../../models/gen';
import { Min } from '../../../models/min';
import { Onc } from '../../../models/onc';
import { Reu } from '../../../models/reu';
import { Tra } from '../../../models/tra';
@Component({
  selector: 'app-consultation',
  templateUrl: './consultation.component.html',
  styleUrls: ['./consultation.component.scss']
})
export class ConsultationComponent implements OnInit {
  user: any;
  info: any;
  @ViewChild('f') floatingLabelForm: NgForm;
  @ViewChild('vform') validationForm: FormGroup;
  @ViewChild('f') registerForm: NgForm;
  his: any;
  name: any;
  specialism: any;
  regularDerForm: FormGroup;
  regularGasForm: FormGroup;
  regularGenForm: FormGroup;
  regularGinForm: FormGroup;
  regularMinForm: FormGroup;
  regularNeuForm: FormGroup;
  regularOncForm: FormGroup;
  regularReuForm: FormGroup;
  regularTraForm: FormGroup;
  isSu: boolean;
  spe: any;
  der: any;
  gas: any;
  gen: any;
  min: any;
  onc: any;
  reu: any;
  tra: any;
  med: any;
  spec: any;
  docName: any;
  doclastName: any;
  lastnamePatient: any;
  namePatient: any;
  constructor(public usersService: UsersService, public patientService: PatientService, private recordService: RecordService, private toastrSerivice: ToastrService, private route: ActivatedRoute, private router: Router, private authService: AuthService) {
    this.namePatient = this.patientService.patientLocal().name;
    this.lastnamePatient = this.patientService.patientLocal().lastName;
    this.spe = this.authService.userLocal().specialism;
    this.docName = this.authService.userLocal().name;
    this.doclastName = this.authService.userLocal().lastName;
    for (let x of this.spe) {
      this.med = x;
      this.spec = this.med.spec;
      if (this.spec == "Dermatology") {
        this.der = true
      }
      if (this.spec == "Gastroenterology") {
        this.gas = true
      }
      if (this.spec == "General") {
        this.gen = true
      }
      if (this.spec == "Internal-Medicine") {
        this.min = true
      }
      if (this.spec == "Oncology") {
        this.onc = true
      }
      if (this.spec == "Rheumatology") {
        this.reu = true
      }
      if (this.spec == "Traumatology") {
        this.tra = true
      }
    }
  }
  ngOnInit() {
    if (this.authService.userLocal().su) {
      this.isSu = true;
      this.router.navigate(['dashboard']);
    };

    this.regularDerForm = new FormGroup({
      'diagDer': new FormControl(null, [Validators.required]),
      'backgroundDer': new FormControl(null, [Validators.required]),
      'motiveDer': new FormControl(null, [Validators.required]),
      'examiningDer': new FormControl(null),
      'docName': new FormControl(this.docName),
      'doclastName': new FormControl(this.doclastName),
      'ta': new FormControl(null),
      'temp': new FormControl(null),
      'weight': new FormControl(null),
      'height': new FormControl(null)
    })
    this.regularGasForm = new FormGroup({
      'diagGas': new FormControl(null, [Validators.required]),
      'backgroundGas': new FormControl(null, [Validators.required]),
      'motiveGas': new FormControl(null, [Validators.required]),
      'examiningGas': new FormControl(null),
      'docName': new FormControl(this.docName),
      'doclastName': new FormControl(this.doclastName),
      'ta': new FormControl(null),
      'temp': new FormControl(null),
      'weight': new FormControl(null),
      'height': new FormControl(null)
    })
    this.regularGenForm = new FormGroup({
      'diagGen': new FormControl(null, [Validators.required]),
      'backgroundGen': new FormControl(null, [Validators.required]),
      'motiveGen': new FormControl(null, [Validators.required]),
      'examiningGen': new FormControl(null),
      'docName': new FormControl(this.docName),
      'doclastName': new FormControl(this.doclastName),
      'ta': new FormControl(null),
      'temp': new FormControl(null),
      'weight': new FormControl(null),
      'height': new FormControl(null),
    })
    /*this.regularGinForm = new FormGroup({

    })*/
    this.regularMinForm = new FormGroup({
      'diagMin': new FormControl(null, [Validators.required]),
      'backgroundMin': new FormControl(null, [Validators.required]),
      'motiveMin': new FormControl(null, [Validators.required]),
      'examiningMin': new FormControl(null),
      'docName': new FormControl(this.docName),
      'doclastName': new FormControl(this.doclastName),
      'ta': new FormControl(null),
      'temp': new FormControl(null),
      'weight': new FormControl(null),
      'height': new FormControl(null)
    })
    /*this.regularNeuForm = new FormGroup({

    })*/
    this.regularOncForm = new FormGroup({
      'diagOnc': new FormControl(null, [Validators.required]),
      'backgroundOnc': new FormControl(null, [Validators.required]),
      'motiveOnc': new FormControl(null, [Validators.required]),
      'examiningOnc': new FormControl(null),
      'docName': new FormControl(this.docName),
      'doclastName': new FormControl(this.doclastName),
      'ta': new FormControl(null),
      'temp': new FormControl(null),
      'weight': new FormControl(null),
      'height': new FormControl(null)
    })
    this.regularReuForm = new FormGroup({
      'diagReu': new FormControl(null, [Validators.required]),
      'backgroundReu': new FormControl(null, [Validators.required]),
      'motiveReu': new FormControl(null, [Validators.required]),
      'examiningReu': new FormControl(null),
      'docName': new FormControl(this.docName),
      'doclastName': new FormControl(this.doclastName),
      'ta': new FormControl(null),
      'temp': new FormControl(null),
      'weight': new FormControl(null),
      'height': new FormControl(null)
    })
    this.regularTraForm = new FormGroup({
      'diagTra': new FormControl(null, [Validators.required]),
      'backgroundTra': new FormControl(null, [Validators.required]),
      'motiveTra': new FormControl(null, [Validators.required]),
      'examiningTra': new FormControl(null),
      'docName': new FormControl(this.docName),
      'doclastName': new FormControl(this.doclastName),
      'ta': new FormControl(null),
      'temp': new FormControl(null),
      'weight': new FormControl(null),
      'height': new FormControl(null)
    })
  }
  resetForm(form?: NgForm) {
    if (form) {
      form.reset();
      this.recordService.selectedDer = new Der();
      this.recordService.selectedGas = new Gas();
      this.recordService.selectedGen = new Gen();
      //this.recordService.selectedGin = new Gin();
      this.recordService.selectedMin = new Min();
      //this.recordService.selectedNeu = new Neu();
      this.recordService.selectedOnc = new Onc();
      this.recordService.selectedReu = new Reu();
      this.recordService.selectedTra = new Tra();
    }
  }
  //Added Recors//
  addRecordDer(form: NgForm) {
    this.route.params.subscribe(data => {
      this.recordService.putRecordDer(data.id, form.value)
        .subscribe(res => {
          this.toastrSerivice.typeSuccess();
          this.resetForm(form);
          console.log(res);
        })
    })
  }
  addRecordGas(form: NgForm) {
    this.route.params.subscribe(data => {
      this.recordService.putRecordGas(data.id, form.value)
        .subscribe(res => {
          this.toastrSerivice.typeSuccess();
          this.resetForm(form);
          console.log(res);
        })
    })
  }
  addRecordGen(form: NgForm) {
    this.route.params.subscribe(data => {
      this.recordService.putRecordGen(data.id, form.value)
        .subscribe(res => {
          this.toastrSerivice.typeSuccess();
          this.resetForm(form);
          console.log(res);
        })
    })
  }
  /*addRecordGin(form: NgForm) {
    this.route.params.subscribe(data => {
      this.recordService.putRecordGin(data.id, form.value)
        .subscribe(res => {
          this.toastrSerivice.typeSuccess();
          this.resetForm(form);
          console.log(res);
        })
    })
  }*/
  addRecordMin(form: NgForm) {
    this.route.params.subscribe(data => {
      this.recordService.putRecordMin(data.id, form.value)
        .subscribe(res => {
          this.toastrSerivice.typeSuccess();
          this.resetForm(form);
          console.log(res);
        })
    })
  }
  /*addRecordNeu(form: NgForm) {
    this.route.params.subscribe(data => {
      this.recordService.putRecordNeu(data.id, form.value)
        .subscribe(res => {
          this.toastrSerivice.typeSuccess();
          this.resetForm(form);
          console.log(res);
        })
    })
  }*/
  addRecordOnc(form: NgForm) {
    this.route.params.subscribe(data => {
      this.recordService.putRecordOnc(data.id, form.value)
        .subscribe(res => {
          this.toastrSerivice.typeSuccess();
          this.resetForm(form);
          console.log(res);
        })
    })
  }

  addRecordReu(form: NgForm) {
    this.route.params.subscribe(data => {
      this.recordService.putRecordReu(data.id, form.value)
        .subscribe(res => {
          this.toastrSerivice.typeSuccess();
          this.resetForm(form);
          console.log(res);
        })
    })
  }
  addRecordTra(form: NgForm) {
    this.route.params.subscribe(data => {
      this.recordService.putRecordTra(data.id, form.value)
        .subscribe(res => {
          this.toastrSerivice.typeSuccess();
          this.resetForm(form);
          console.log(res);
        })
    })
  }
  //ADDED Record END//  
}
