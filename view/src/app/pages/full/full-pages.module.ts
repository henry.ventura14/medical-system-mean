import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CustomFormsModule } from 'ng2-validation';
import { MatchHeightModule } from "../../shared/directives/match-height.directive";
import { FullPagesRoutingModule } from "./full-pages-routing.module";

import { DashboardComponent } from './dashboard/dashboard.component';
import { HelpComponent } from './help/help.component';
import { PatientComponent } from './patient/patient.component';
import { ReportsComponent } from './reports/reports.component';
import { DetailPatientComponent } from './detail-patient/detail-patient.component';
import { ConsultationComponent } from './consultation/consultation.component';
import { AdminComponent } from './admin/admin.component'

import { SearchUser } from '../../pipes/search.user.pipe';
import { SearchLog } from '../../pipes/search.log.pipe';
import { RecordService } from '../../services/record.service';
import { SearchConsult } from 'app/pipes/search.consult.pipe';
import { SearchDateConsult } from 'app/pipes/search.date-consult.pipe';

@NgModule({
    imports: [
        CommonModule,
        FullPagesRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        MatchHeightModule,
        CustomFormsModule,
        NgbModule
    ],
    declarations: [
        AdminComponent,
        DashboardComponent,
        HelpComponent,
        PatientComponent,
        ReportsComponent,
        DetailPatientComponent,
        ConsultationComponent,
        SearchUser,
        SearchLog,
        SearchConsult,
        SearchDateConsult
        
    ],
    providers: [
        RecordService
    ]
})
export class FullPagesModule { }
