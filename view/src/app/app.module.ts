import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from "./shared/shared.module";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { ToastModule, ToastOptions } from 'ng2-toastr';
import { BrowserModule } from '@angular/platform-browser';
import { CustomOption } from "./shared/toastr/custom-option";
import { AppComponent } from './app.component';

import { ContentLayoutComponent } from "./layouts/content/content-layout.component";
import { FullLayoutComponent } from "./layouts/full/full-layout.component";

import { ToastrService } from './shared/toastr/toastr.service';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './services/auth-guard.service';
import { UsersService } from './services/users.service';
import { LoggerService } from './services/logger.service';
import { MessageService } from './services/email.service';
import { PatientService } from './services/patients.service';
//import * as $ from 'jquery';

@NgModule({
    declarations: [
        AppComponent,
        FullLayoutComponent,
        ContentLayoutComponent
    ],
    imports: [
        BrowserAnimationsModule,
        AppRoutingModule,
        SharedModule,
        ToastModule,
        HttpClientModule,
        BrowserModule,
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        ToastModule.forRoot(),
        NgbModule.forRoot()
    ],
    providers: [
        PatientService,
        UsersService,
        LoggerService,
        MessageService,
        AuthService,
        AuthGuard,
        ToastrService,
        { provide: ToastOptions, useClass: CustomOption }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }