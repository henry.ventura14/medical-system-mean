import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders} from '@angular/common/http';

@Injectable()
export class LoggerService {

  readonly URL_API = 'http://localhost:3000/api/logger';

  constructor(private http: HttpClient) {
  }

  getLogs() {
    return this.http.get(this.URL_API);
  }
  
  postLog(log:any) {
    return this.http.post(this.URL_API, log)
  }

}