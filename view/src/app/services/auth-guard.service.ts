import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
 body:any;
  constructor(private authService: AuthService, private router: Router) {}

  canActivate() {
    if (!this.authService.login(this.body)) {
      this.router.navigateByUrl('dashboard');
      return false;
    }
    return true;
  }
}