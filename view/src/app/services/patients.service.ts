import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Patient } from '../models/patients';

@Injectable()
export class PatientService {
  
  selectedPatient: Patient;
  patient: Patient[];
  readonly URL_API = 'http://localhost:3000/api/patients';

  patientLocal() {
    return JSON.parse(localStorage.getItem('Patient'));
  }

  constructor(public http: HttpClient) {
    this.selectedPatient = new Patient();
  }

  getPatients() {
    return this.http.get(this.URL_API);
  }

  getPatient(_id: string) {
    return this.http.get(this.URL_API + `/${_id}`);
  }

  postPatient(patient: Patient) {
    return this.http.post(this.URL_API, patient);
  }

  putPatient(_id: string, patient: Patient) {
    return this.http.put(this.URL_API + `/${_id}`, patient);
  }

  deletePatient(_id: string) {
    return this.http.delete(this.URL_API + `/${_id}`);
  }

}
