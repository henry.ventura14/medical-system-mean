import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders} from '@angular/common/http';
@Injectable()
export class MessageService {
constructor(private http: HttpClient) { }
sendMessage(body) {
 return this.http.post('http://localhost:3000/mailer', body);
 }
}