import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Der } from '../models/der';
import { Gas } from '../models/gas';
import { Gen } from '../models/gen';
import { Gin } from '../models/gin';
import { Min } from '../models/min';
import { Neu } from '../models/neu';
import { Onc } from '../models/onc';
import { Reu } from '../models/reu';
import { Tra } from '../models/tra';

import { Patient } from '../models/patients';
import { Record } from '../models/record';

@Injectable()

export class RecordService {
  selectedDer: Der;
  selectedGas: Gas;
  selectedGen: Gen;
  selectedGin: Gin;
  selectedMin: Min;
  selectedNeu: Neu;
  selectedOnc: Onc;
  selectedReu: Reu;
  selectedTra: Tra;
  patient: Patient[];
  record: Record[];
  readonly URL_API = 'http://localhost:3000/api/records';

  constructor(public http: HttpClient) {

    this.selectedDer = new Der();
    this.selectedGas = new Gas();
    this.selectedGen = new Gen();
    this.selectedGin = new Gin();
    this.selectedMin = new Min();
    this.selectedNeu = new Neu();
    this.selectedOnc = new Onc();
    this.selectedReu = new Reu();
    this.selectedTra = new Tra();
  }

  getStories() {
    return this.http.get(this.URL_API);
  }
  //GETINIT//
  getRecord(id: string) {
    return this.http.get(this.URL_API + `/${id}`);
  }

  //GET INIT//

  postRecord(id: string) {
    return this.http.post(this.URL_API + `/${id}`, null);
  }

  //Put INIT//
  putRecordDer(id: string, der: Der) {
    return this.http.put(this.URL_API + `/der/${id}`, der);
  }
  putRecordGas(id: string, gas: Gas) {
    return this.http.put(this.URL_API + `/gas/${id}`, gas);
  }
  putRecordGen(id: string, gen: Gen) {
    return this.http.put(this.URL_API + `/gen/${id}`, gen);
  }
  putRecordGin(id: string, gin: Gin) {
    return this.http.put(this.URL_API + `/gin/${id}`, gin);
  }
  putRecordMin(id: string, min: Min) {
    return this.http.put(this.URL_API + `/min/${id}`, min);
  }
  putRecordNeu(id: string, neu: Neu) {
    return this.http.put(this.URL_API + `/neu/${id}`, neu);
  }
  putRecordOnc(id: string, onc: Onc) {
    return this.http.put(this.URL_API + `/onc/${id}`, onc);
  }
  putRecordReu(id: string, reu: Reu) {
    return this.http.put(this.URL_API + `/reu/${id}`, reu);
  }
  putRecordTra(id: string, tra: Tra) {
    return this.http.put(this.URL_API + `/tra/${id}`, tra);
  }
  //Put END//

  deleteRecord(_id: string) {
    return this.http.delete(this.URL_API + `/${_id}`);
  }

}