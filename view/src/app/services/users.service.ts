import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../models/users';
@Injectable()
export class UsersService {
  selectedUser: User;
  users: User[];
  readonly URL_API = 'http://localhost:3000/api/users';
  Iniciar
  constructor(private http: HttpClient) {
    this.selectedUser = new User();
  }

  getUsers() {
    return this.http.get(this.URL_API, {
      observe: 'body',
      withCredentials: true,
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    });
  }
  getUser(id: string) {
    return this.http.get(this.URL_API + `/${id}`, {
      observe: 'body',
      withCredentials: true,
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    });
  }
  postUser(User: User) {
    return this.http.post(this.URL_API, User, {
      observe: 'body',
      withCredentials: true,
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    });
  }
  email(body: any) {
    return this.http.post(this.URL_API + '/email', body, {
      observe: 'body',
      withCredentials: true,
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    });
  }
  backup(body: any) {
    return this.http.post(this.URL_API + '/backup', body, {
      observe: 'body',
      withCredentials: true,
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    });
  }
  restore(body: any) {
    return this.http.post(this.URL_API + '/restore', body, {
      observe: 'body',
      withCredentials: true,
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    });
  }
  unlockUser(_id: string) {
    return this.http.put(this.URL_API + '/unlock' + `/${_id}`, null, {
      observe: 'body',
      withCredentials: true,
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    });
  }
  changePassword(_id: string, user: User) {
    return this.http.put(this.URL_API + '/change' + `/${_id}`, user, {
      observe: 'body',
      withCredentials: true,
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    });
  }
  typeUserMed(_id: string) {
    return this.http.put(this.URL_API + '/med' + `/${_id}`, null, {
      observe: 'body',
      withCredentials: true,
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    });
  }
  typeUserSu(_id: string) {
    return this.http.put(this.URL_API + '/su' + `/${_id}`, null, {
      observe: 'body',
      withCredentials: true,
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    });
  }
  forgotPassword(_id: string, user: User) {
    return this.http.put(this.URL_API + '/forgot' + `/${_id}`, user, {
      observe: 'body',
      withCredentials: true,
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    });
  }
  putUser(_id: string, user: User) {
    return this.http.put(this.URL_API + `/${_id}`, user, {
      observe: 'body',
      withCredentials: true,
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    });
  }
  deleteUser(_id: string) {
    return this.http.delete(this.URL_API + `/${_id}`, {
      observe: 'body',
      withCredentials: true,
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    });
  }
}