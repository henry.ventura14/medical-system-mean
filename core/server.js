var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var moment = require('moment-timezone');
var indexRouter = require('./routes/index.routes');
var authRouter = require('./routes/auth.routes');
var cors = require('cors');
var bodyParser = require('body-parser');
var app = express();
const Mailer = require('./config/nodeMeiler');
moment().tz("America/Caracas").format('ha z');
app.use(cors({
  origin: ['http://localhost:4200', 'http://127.0.0.1:4200'],
  credentials: true
}));
var mongoose = require('mongoose');
var URI = 'mongodb://yourUser:password@localhost:27017/db';
//var URI = 'mongodb://yourUser:Password@12345.mlab.com:port/db';
mongoose.connect(URI, {
  useNewUrlParser: true,
  useCreateIndex: true
});
//passport
var passport = require('passport');
var session = require('express-session');
const MongoStore = require('connect-mongo')(session);
app.use(session({
  name: 'myname.sid',
  resave: false,
  saveUninitialized: false,
  secret: 'secret',
  cookie: {
    maxAge: 36000000,
    httpOnly: false,
    secure: false
  },
  store: new MongoStore({ mongooseConnection: mongoose.connection })
}));
require('./config/passport');
app.use(passport.initialize());
app.use(passport.session());
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
app.use(logger('dev'));
app.use(express.json());
app.use(bodyParser.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, './dist')));
app.use('/', indexRouter);
app.use('/users', authRouter);
app.post('/mailer', (req, res) => {
  Mailer(req.body);
  res.status(200).send();
})
app.use('/api/users', require('./routes/user.routes'));
app.use('/api/patients', require('./routes/patient.routes'));
app.use('/api/records', require('./routes/record.routes'));
app.use('/api/logger', require('./routes/logger.routes'));
app.use(function (req, res) {
  res.sendFile(path.join(__dirname, '/dist', 'index.html'));
});
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});
// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
