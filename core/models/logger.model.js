const mongoose = require('mongoose');
const {
    Schema
} = mongoose;

const LoggerSchema = new Schema({
    action: {
        type: String,
        required: true
    },
    user: {
        type: String,
        required: false,
        default: "Anonimus"
    },
    email: {
        type: String,
        required: true
    },
    ip: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    }
}, {
        timestamps: {
            createdAt: 'created_at'
        }
    });

module.exports = mongoose.model('Logger', LoggerSchema);