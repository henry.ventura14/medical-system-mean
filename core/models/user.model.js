var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt');

const UserSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  typeDocument: {
    type: String,
    required: true
  },
  documentNumber: {
    type: String,
    required: true
  },
  specialism: {
    type: Array,
    required: true
  },
  phone: {
    type: String,
    required: true
  },
  direction: {
    type: String,
    required: true
  },
  user: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  question: {
    type: String,
    required: true
  },
  answer: {
    type: String,
    required: true
  },
  attemps: {
    type: Number,
    min: 0,
    max: 1,
    required: false
  },
  block: {
    type: Boolean,
    required: false,
    default: true
  },
  su: {
    type: Boolean,
    required: false,
    default: false
  },
  med: {
    type: Boolean,
    required: false,
    default: true
  }
}, {
    timestamps: {
      createdAt: 'created_at'
    }
  });

UserSchema.statics.hashPassword = function hashPassword(password) {
  return bcrypt.hashSync(password, 10);
}

UserSchema.methods.isValid = function (hashedpassword) {
  return bcrypt.compareSync(hashedpassword, this.password);
}

UserSchema.statics.hashAnswer = function hashAnswer(answer) {
  return bcrypt.hashSync(answer, 10);
}

UserSchema.methods.isValidAnswer = function (hashedAnswer) {
  return bcrypt.compareSync(hashedAnswer, this.answer);
}



module.exports = mongoose.model('User', UserSchema);