const mongoose = require('mongoose');
const {
    Schema
} = mongoose;

const RecordSchema = new Schema({
    "_id": {
        type: String,
    },
    "der": [{
        "_id": {
            type: String,
        },
        dateConsult: {
            type: Date,
            default: Date.now
        },
        diagDer: {
            type: String
        },
        motiveDer: {
            type: String,
            required: true
        },
        docName: {
            type: String,
            required: true
        },
        doclastName: {
            type: String,
            required: true
        },
        ta: {
            type: String,
        },
        temp: {
            type: String,
        },
        height: {
            type: String,
        },
        weight: {
            type: String,
        },
        spc: {
            type: String,
            default: "Dermatology"

        },
        backgroundDer: {
            type: String,
            required: true
        },
        examiningDer: {
            type: String,
            required: false
        }
    }],
    "gas": [{
        "_id": {
            type: String,
        },
        dateConsult: {
            type: Date,
            default: Date.now
        },
        diagGas: {
            type: String
        },
        motiveGas: {
            type: String,
            required: true
        },
        docName: {
            type: String,
            required: true
        },
        doclastName: {
            type: String,
            required: true
        },
        ta: {
            type: String,
        },
        temp: {
            type: String,
        },
        height: {
            type: String,
        },
        weight: {
            type: String,
        },
        spc: {
            type: String,
            default: "Gastroenterology"

        },
        backgroundGas: {
            type: String,
            required: true
        },
        examiningGas: {
            type: String,
            required: false
        }
    }],
    "gen": [{
        "_id": {
            type: String,
        },
        dateConsult: {
            type: Date,
            default: Date.now
        },
        diagGen: {
            type: String,
            required: true
        },
        motiveGen: {
            type: String,
            required: true
        },
        docName: {
            type: String,
            required: true
        },
        doclastName: {
            type: String,
            required: true
        },
        ta: {
            type: String,
        },
        temp: {
            type: String,
        },
        height: {
            type: String,
        },
        weight: {
            type: String,
        },
        spc: {
            type: String,
            default: "M General"

        },
        backgroundGen: {
            type: String,
            required: true
        },
        examiningGen: {
            type: String,
            required: false
        }
    }],
    "gin": [{
        "_id": {
            type: String,
        },
        dateConsult: {
            type: Date,
            default: Date.now
        },
        diagGin: {
            type: String
        },
        motiveGin: {
            type: String,
            required: true
        },
        docName: {
            type: String,
            required: true
        },
        doclastName: {
            type: String,
            required: true
        },
        ta: {
            type: String,
        },
        temp: {
            type: String,
        },
        height: {
            type: String,
        },
        weight: {
            type: String,
        },
        spc: {
            type: String,
            default: "Gynecology"

        },
        backgroundGin: {
            type: String,
            required: true
        },
        examiningGin: {
            type: String,
            required: false
        }
    }],
    "min": [{
        "_id": {
            type: String,
        },
        dateConsult: {
            type: Date,
            default: Date.now
        },
        diagMin: {
            type: String
        },
        motiveMin: {
            type: String,
            required: true
        },
        docName: {
            type: String,
            required: true
        },
        doclastName: {
            type: String,
            required: true
        },
        ta: {
            type: String,
        },
        temp: {
            type: String,
        },
        height: {
            type: String,
        },
        weight: {
            type: String,
        },
        spc: {
            type: String,
            default: "M Internal"

        },
        backgroundMin: {
            type: String,
            required: true
        },
        examiningMin: {
            type: String,
            required: false
        }
    }],
    "neu": [{
        "_id": {
            type: String,
        },
        dateConsult: {
            type: Date,
            default: Date.now
        },
        diagNeu: {
            type: String
        },
        motiveNeu: {
            type: String,
            required: true
        },
        docName: {
            type: String,
            required: true
        },
        doclastName: {
            type: String,
            required: true
        },
        ta: {
            type: String,
        },
        temp: {
            type: String,
        },
        height: {
            type: String,
        },
        weight: {
            type: String,
        },
        spc: {
            type: String,
            default: "Neumonology"

        },
        backgroundNeu: {
            type: String,
            required: true
        },
        examiningNeu: {
            type: String,
            required: false
        }
    }],
    "onc": [{
        "_id": {
            type: String,
        },
        dateConsult: {
            type: Date,
            default: Date.now
        },
        diagOnc: {
            type: String
        },
        motiveOnc: {
            type: String,
            required: true
        },
        docName: {
            type: String,
            required: true
        },
        doclastName: {
            type: String,
            required: true
        },
        ta: {
            type: String,
        },
        temp: {
            type: String,
        },
        height: {
            type: String,
        },
        weight: {
            type: String,
        },
        spc: {
            type: String,
            default: "Oncology"

        },
        backgroundOnc: {
            type: String,
            required: true
        },
        examiningOnc: {
            type: String,
            required: false
        }
    }],
    "reu": [{
        "_id": {
            type: String,
        },
        dateConsult: {
            type: Date,
            default: Date.now
        },
        diagReu: {
            type: String
        },
        motiveReu: {
            type: String,
            required: true
        },
        docName: {
            type: String,
            required: true
        },
        doclastName: {
            type: String,
            required: true
        },
        ta: {
            type: String,
        },
        temp: {
            type: String,
        },
        height: {
            type: String,
        },
        weight: {
            type: String,
        },
        spc: {
            type: String,
            default: "Rheumatology"

        },
        backgroundReu: {
            type: String,
            required: true
        },
        examiningReu: {
            type: String,
            required: false
        }
    }],
    "tra": [{
        "_id": {
            type: String,
        },
        dateConsult: {
            type: Date,
            default: Date.now
        },
        diagTra: {
            type: String
        },
        motiveTra: {
            type: String,
            required: true
        },
        docName: {
            type: String,
            required: true
        },
        doclastName: {
            type: String,
            required: true
        },
        ta: {
            type: String,
        },
        temp: {
            type: String,
        },
        height: {
            type: String,
        },
        weight: {
            type: String,
        },
        spc: {
            type: String,
            default: "Traumatology"

        },
        backgroundTra: {
            type: String,
            required: true
        },
        examiningTra: {
            type: String,
            required: false
        }
    }]
}, {
        timestamps: {
            createdAt: 'created_at'
        }
    });

module.exports = mongoose.model('Record', RecordSchema);