const mongoose = require('mongoose');
const Logger = require('../models/logger.model');

const {
  Schema
} = mongoose;

const PatientSchema = new Schema({

  name: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  typeDocument: {
    type: String,
    required: true
  },
  documentNumber: {
    type: String,
    required: true
  },
  birth: {
    type: String,
    required: true
  },
  gender: {
    type: String,
    required: true
  },
  phone: {
    type: String,
    required: true
  },
  direction: {
    type: String,
    required: false
  },
  plan: {
    type: String,
    required: false
  }

}, {
    timestamps: {
      createdAt: 'created_at'
    }
  });

module.exports = mongoose.model('Patient', PatientSchema);