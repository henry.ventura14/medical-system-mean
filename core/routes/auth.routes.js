var express = require('express');
var router = express.Router();
var User = require('../models/user.model');
var passport = require('passport');
/* GET users listing. 
// router.get('/', function(req, res, next) {
//   res.send('respond with a resource');
// });

router.post('/register', function (req, res, next) {
  addToDB(req, res);
});


async function addToDB(req, res) {

  var user = new User({
    email: req.body.email,
    username: req.body.username,
    password: User.hashPassword(req.body.password),
    creation_dt: Date.now()
  });

  try {
    doc = await user.save();
    return res.status(201).json(doc);
  }
  catch (err) {
    return res.status(501).json(err);
  }
}

*/
router.post('/login', async (req, res, next) => {
  passport.authenticate('local', async (err, user, info) => {
    const user_1 = await User.findOne({ "email": req.body.email });
    if (err) { return res.status(501).json(err); }
    if (user_1 && !user && user_1.attemps < 3) {
      user_1.attemps = (isNaN(user_1.attemps)) ? 1 : user_1.attemps += 1;
      User.update({ _id: user_1._id }, { $set: user_1 }, (err, user) => { console.log(err, user) });
      return res.status(501).json({ attemps: user_1.attemps });
    }
    if (user_1 && !user && user_1.attemps > 2) {
      user_1.block = true;
      User.update({ _id: user_1._id }, { $set: user_1 }, (err, user) => { console.log(err, user) });
      return res.status(501).json({ block: user_1.block });
    }
    if (!user) {
      return res.status(501).json(info);
    }
    req.logIn(user, function (err) {
      if (user_1 && !user_1.block) {
        user_1.attemps = 0;
        User.update({ _id: user_1._id }, { $set: user_1 }, function (err, user) { console.log(err, user) });
      }
      if (err || user_1.block) { return res.status(501).json({ block: user_1.block }) };
      return res.status(200).json({
        user: user_1
      });
    });
  })(req, res, next);
});

router.get('/user', isValidUser, function (req, res, next) {
  return res.status(200).json(req.user);
});

router.get('/logout', isValidUser, function (req, res, next) {
  req.logout();
  return res.status(200).json({ message: 'Logout Success' });
})

function isValidUser(req, res, next) {
  if (req.isAuthenticated()) next();
  else return res.status(401).json({ message: 'Unauthorized Request' });
}

module.exports = router;
