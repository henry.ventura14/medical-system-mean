const express = require('express');
const router = express.Router();
const log = require('../controllers/logger.controller');

router.get('/', log.getLoggers);

router.post('/', log.createLogger);

module.exports = router;