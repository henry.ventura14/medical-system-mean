const express = require('express');
const router = express.Router();

const record = require('../controllers/record.controller');

router.get('/', record.getStories);

router.post('/:id', record.createRecord);

router.get('/:id', record.getRecord);

router.put('/der/:id', record.editRecordDer);

router.put('/gas/:id', record.editRecordGas);

router.put('/gen/:id', record.editRecordGen);

router.put('/gin/:id', record.editRecordGin);

router.put('/min/:id', record.editRecordMin);

router.put('/neu/:id', record.editRecordNeu);

router.put('/onc/:id', record.editRecordOnc);

router.put('/reu/:id', record.editRecordReu);

router.put('/tra/:id', record.editRecordTra);

router.delete('/:id', record.deleteRecord);

module.exports = router; 