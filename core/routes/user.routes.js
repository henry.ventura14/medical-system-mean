const express = require('express');
const router = express.Router();
const user = require('../controllers/user.controller');
router.get('/', user.getUsers);
router.post('/', user.createUser);
router.post('/email', user.mailUser);
router.put('/change/:id', user.ChangePass);
router.put('/forgot/:id', user.ForgotPass);
router.post('/backup', user.CreateBackup);
router.post('/restore', user.CreateRestore);
router.get('/:id', user.getUser);
router.put('/:id', user.editUser);
router.put('/med/:id', user.editUserMed);
router.put('/su/:id', user.editUserSu);
router.put('/unlock/:id', user.unlockUser);
router.delete('/:id', user.deleteUser);
module.exports = router;



