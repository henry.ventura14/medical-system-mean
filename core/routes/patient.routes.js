const express = require('express');
const router = express.Router();

const patient = require('../controllers/patient.controller');

router.get('/', patient.getPatients);

router.get('/:id', patient.getPatient);

router.post('/', patient.createPatient);

router.put('/:id', patient.editPatient);

router.delete('/:id', patient.deletePatient);

module.exports = router;