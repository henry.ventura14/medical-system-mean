const Record = require('../models/record.model');
const RecordCtrl = {};

RecordCtrl.getStories = async (req, res) => {
  const record = await Record.find();
  res.json(record);
};

RecordCtrl.createRecord = async (req, res) => {
  const {
    id
  } = req.params;
  const record = new Record({
    "_id": id
  });
  const get = await Record.findById(id);
  if (get == null || get["_id"] != id) {
    await record.save();
    res.json({
      status: 'Record Saved'
    });
  } else {
    res.json({
      status: 'Record Ready'
    });
  }
};

RecordCtrl.getRecord = async (req, res) => {
  const {
    id
  } = req.params;
  const record = await Record.findById(id);
  res.json(record);
};

//UPDATES Record INIT//

RecordCtrl.editRecordDer = async (req, res) => {
  const {
    id
  } = req.params;
  await Record.findByIdAndUpdate(id, {
    $push: {
      "der": [{
        "_id":id,
        "diagDer": req.body.diagDer,
        "motiveDer": req.body.motiveDer,
        "backgroundDer": req.body.backgroundDer,
        "examiningDer": req.body.examiningDer,
        "docName": req.body.docName,
        "doclastName": req.body.doclastName,
        "ta":req.body.ta,
        "temp":req.body.temp,
        "weight":req.body.weight,
        "height":req.body.height
      }]
    }
  }, {
    new: true
  });
  res.json({
    status: 'Record Update'
  });
};

RecordCtrl.editRecordGas = async (req, res) => {
  const {
    id
  } = req.params;
  await Record.findByIdAndUpdate(id, {
    $push: {
      "gas": [{
        "_id":id,
        "diagGas": req.body.diagGas,
        "motiveGas": req.body.motiveGas,
        "backgroundGas": req.body.backgroundGas,
        "examiningGas": req.body.examiningGas,
        "docName": req.body.docName,
        "doclastName": req.body.doclastName,
        "ta":req.body.ta,
        "temp":req.body.temp,
        "weight":req.body.weight,
        "height":req.body.height
      }]
    }
  }, {
    new: true
  });
  res.json({
    status: 'Record Update'
  });
};

RecordCtrl.editRecordGen = async (req, res) => {
  const {
    id
  } = req.params;
  await Record.findByIdAndUpdate(id, {
    $push: {
      "gen": [{
        "_id":id,
        "diagGen": req.body.diagGen,
        "motiveGen": req.body.motiveGen,
        "backgroundGen": req.body.backgroundGen,
        "examiningGen": req.body.examiningGen,
        "docName": req.body.docName,
        "doclastName": req.body.doclastName,
        "ta":req.body.ta,
        "temp":req.body.temp,
        "weight":req.body.weight,
        "height":req.body.height
      }]
    }
  }, {
    new: true
  });
  res.json({
    status: 'Record Update'
  });
};

RecordCtrl.editRecordGin = async (req, res) => {
  const {
    id
  } = req.params;
  await Record.findByIdAndUpdate(id, {
    $push: {
      "gin": [{
        "_id":id,
        "diagGin": req.body.diagGin,
        "motiveGin": req.body.motiveGin,
        "backgroundGin": req.body.backgroundGin,
        "examiningGin": req.body.examiningGin,
        "ta":req.body.ta,
        "temp":req.body.temp,
        "weight":req.body.weight,
        "height":req.body.height
      }]
    }
  }, {
    new: true
  });
  res.json({
    status: 'Record Update'
  });
};

RecordCtrl.editRecordMin = async (req, res) => {
  const {
    id
  } = req.params;
  await Record.findByIdAndUpdate(id, {
    $push: {
      "min": [{
        "_id":id,
        "diagMin": req.body.diagMin,
        "motiveMin": req.body.motiveMin,
        "backgroundMin": req.body.backgroundMin,
        "examiningMin": req.body.examiningMin,
        "docName": req.body.docName,
        "doclastName": req.body.doclastName,
        "ta":req.body.ta,
        "temp":req.body.temp,
        "weight":req.body.weight,
        "height":req.body.height
      }]
    }
  }, {
    new: true
  });
  res.json({
    status: 'Record Update'
  });
};

RecordCtrl.editRecordNeu = async (req, res) => {
  const {
    id
  } = req.params;
  await Record.findByIdAndUpdate(id, {
    $push: {
      "neu": [{
        "_id":id,
        "diagNeu": req.body.diagNeu,
        "motiveNeu": req.body.motiveNeu,
        "backgroundNeu": req.body.backgroundNeu,
        "examiningNeu": req.body.examiningNeu,
        "ta":req.body.ta,
        "temp":req.body.temp,
        "weight":req.body.weight,
        "height":req.body.height
      }]
    }
  }, {
    new: true
  });
  res.json({
    status: 'Record Update'
  });
};

RecordCtrl.editRecordOnc = async (req, res) => {
  const {
    id
  } = req.params;
  await Record.findByIdAndUpdate(id, {
    $push: {
      "onc": [{
        "_id":id,
        "diagOnc": req.body.diagOnc,
        "motiveOnc": req.body.motiveOnc,
        "backgroundOnc": req.body.backgroundOnc,
        "examiningOnc": req.body.examiningOnc,
        "docName": req.body.docName,
        "doclastName": req.body.doclastName,
        "ta":req.body.ta,
        "temp":req.body.temp,
        "weight":req.body.weight,
        "height":req.body.height
      }]
    }
  }, {
    new: true
  });
  res.json({
    status: 'Record Update'
  });
};


RecordCtrl.editRecordReu = async (req, res) => {
  const {
    id
  } = req.params;
  await Record.findByIdAndUpdate(id, {
    $push: {
      "reu": [{
        "_id":id,
        "diagReu": req.body.diagReu,
        "motiveReu": req.body.motiveReu,
        "backgrounReu": req.body.backgroundReu,
        "examiningReu": req.body.examiningReu,
        "docName": req.body.docName,
        "doclastName": req.body.doclastName,
        "ta":req.body.ta,
        "temp":req.body.temp,
        "weight":req.body.weight,
        "height":req.body.height
      }]
    }
  }, {
    new: true
  });
  res.json({
    status: 'Record Update'
  });
};

RecordCtrl.editRecordTra = async (req, res) => {
  const {
    id
  } = req.params;
  await Record.findByIdAndUpdate(id, {
    $push: {
      "tra": [{
        "_id":id,
        "diagTra": req.body.diagTra,
        "motiveTra": req.body.motiveTra,
        "backgroundTra": req.body.backgroundTra,
        "examiningTra": req.body.examiningTra,
        "docName": req.body.docName,
        "doclastName": req.body.doclastName,
        "ta":req.body.ta,
        "temp":req.body.temp,
        "weight":req.body.weight,
        "height":req.body.height
      }]
    }
  }, {
    new: true
  });
  res.json({
    status: 'Record Update'
  });
};
//UPDATES Record END//

RecordCtrl.deleteRecord = async (req, res) => {
  await Record.findByIdAndRemove(req.params.id);
  res.json({
    status: 'Record Delete'
  });
};

module.exports = RecordCtrl;