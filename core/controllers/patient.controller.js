const Patient = require('../models/patient.model');
const patientCtrl = {};

patientCtrl.getPatients = async (req, res) => {
  const patients = await Patient.find();
  res.json(patients);
};

patientCtrl.createPatient = async (req, res, err) => {
  const patient = new Patient(req.body);
  const get = await Patient.findOne({ "documentNumber": patient.documentNumber, "typeDocument": patient.typeDocument });
  if (!get) {
    await patient.save();
    res.json({
      status: 'patient saved'
    });
  }
  if (err) { return res.status(501).json(err); }
};

patientCtrl.getPatient = async (req, res, netx) => {
  const {
    id
  } = req.params;
  const patient = await Patient.findById(id);
  res.json(patient);
};

patientCtrl.editPatient = async (req, res) => {
  const {
    id
  } = req.params;
  const patient = {
    name: req.body.name,
    lastName: req.body.lastName,
    typeDocument: req.body.typeDocument,
    documentNumber: req.body.documentNumber,
    birth: req.body.birth,
    gender: req.body.gender,
    phone: req.body.phone,
    direction: req.body.direction,
    plan: req.body.plan
  };
  await Patient.findOneAndUpdate(id, {
    $set: patient
  }, {
      new: true
    });

  res.json({
    status: 'patient Update'
  });
};

patientCtrl.deletePatient = async (req, res) => {
  await Patient.findByIdAndRemove(req.params.id);
  res.json({
    status: 'patient Delete'
  });
};

module.exports = patientCtrl;
