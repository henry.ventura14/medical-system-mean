const Logger = require('../models/logger.model');
const address = require('address');
const loggerCtrl = {};
address.ip();
address.ip('lo');

loggerCtrl.getLoggers = async (req, res) => {
  const logger = await Logger.find();
  res.json(logger);
};

loggerCtrl.createLogger = async (req, res, err) => {
  const logger = new Logger({
    user: req.body.user,
    email: req.body.email,
    action: req.body.action,
    ip: address.ip()
  }); 
  await logger.save();
  res.json({
    status: 'logger saved'
  });
};

module.exports = loggerCtrl;