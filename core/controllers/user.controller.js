const User = require('../models/user.model');
const back = require('mongodb-backup');
const rest = require('mongodb-restore');
const bcrypt = require('bcrypt');
const path = require('path');

const userCtrl = {};
userCtrl.getUsers = async (req, res, err) => {
  const user = await User.find();
  res.json(user);
};
userCtrl.createUser = async (req, res, err) => {
  const user = new User(req.body);
  const get = await User.findOne({
    "email": user.email,
    "user": user.user,
    "typeDocument": user.typeDocument,
    "documentNumber": user.documentNumber
  });
  user.password = User.hashPassword(req.body.passwordA);
  user.answer = User.hashAnswer(req.body.answer);
  if (!get) {
    await user.save();
    res.json({
      status: 'user saved'
    });
  }
  if (err) { return res.status(501).json(err); }
};
userCtrl.getUser = async (req, res) => {
  const user = await User.findById(req.params.id);
  res.json(user);
};
userCtrl.mailUser = async (req, res, err) => {
  const user = await User.findOne({ "email": req.body.email })
  if (user) {
    res.json(user);
  }
  if (err) { return res.status(501).json(err); }
};
userCtrl.editUser = async (req, res) => {
  const {
    id
  } = req.params;
  const user = {
    name: req.body.name,
    lastName: req.body.lastName,
    typeDocument: req.body.typeDocument,
    documentNumber: req.body.documentNumber,
    user: req.body.user,
    email: req.body.email,
    phone: req.body.phone,
    direction: req.body.direction
  };
  await User.findByIdAndUpdate(id, {
    $set: user
  }, {
      new: true
    });
  res.json({
    status: 'User Update'
  });
};
userCtrl.editUserMed = async (req, res) => {
  const {
    id
  } = req.params;
  await User.findByIdAndUpdate(id, {
    $set: {
      su: false,
      med: true
    }
  }, {
      new: true
    });
  res.json({
    status: 'User type change to med'
  });
};

userCtrl.editUserSu = async (req, res) => {
  const {
    id
  } = req.params;
  await User.findByIdAndUpdate(id, {
    $set: {
      su: true,
      med: false
    }
  }, {
      new: true
    });
  res.json({
    status: 'User type change to su'
  });
};
userCtrl.unlockUser = async (req, res) => {
  const {
    id
  } = req.params;
  await User.findByIdAndUpdate(id, {
    $set: {
      block: false,
      attemps: 0
    }
  }, {
      new: true
    });
  res.json({
    status: 'User Update'
  });
};

userCtrl.deleteUser = async (req, res, err) => {
  await User.findByIdAndRemove(req.params.id);
  res.json({
    status: 'User Delete'
  });
  if (err) { return res.status(501).json(err); }
};
userCtrl.CreateBackup = async (req, res) => {

  await back({
    uri: 'mongodb://root:1423henry@localhost:27017/ipp',
    root: __dirname+"/../db",
    tar: 'dump.tar'
  });
  res.json({
    status: 'backup saved'
  });
};
userCtrl.CreateRestore = async (req, res) => {
  await rest({
    uri: 'mongodb://root:1423henry@localhost:27017/ipp',
    root: __dirname,
    tar: req.body.nameRestore
  });
  res.json({
    status: 'restore'
  });
};
userCtrl.ChangePass = async (req, res, err) => {
  const {
    id
  } = req.params;
  const user = req.body;
  user.passwordA = User.hashPassword(req.body.passwordA);
  const pass = await User.findById(id);
  const match = await bcrypt.compare(user.passwordOld, pass.password);
  if (match) {
    await User.findByIdAndUpdate(id, {
      $set: {
        password: user.passwordA
      }
    }, {
        new: true
      });
    res.json({ status: "change password" });
  } if (!match) {
    return res.status(501).json(err);
  }
}
userCtrl.ForgotPass = async (req, res, err) => {
  const {
    id
  } = req.params;
  const user = req.body;
  const pass = await User.findById(id);
  user.passwordA = User.hashPassword(req.body.passwordA);
  const match = await bcrypt.compare(user.answer, pass.answer);
  if (match) {
    await User.findByIdAndUpdate(id, {
      $set: {
        password: user.passwordA
      }
    }, {
        new: true
      });
    res.json({ status: "change password forgot" });
  } if (!match) {
    return res.status(501).json(err);
  }
}
module.exports = userCtrl;
