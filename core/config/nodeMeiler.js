const nodemailer = require('nodemailer');
module.exports = (form) => {
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: '', 
            pass: '' 
        }
    });
    const mailOptions = {
        from: `”${form.user} ” <${form.email}>`,
        to: `${form.email}`, 
        subject: 'Respado de datos IppSystem',
        html: `
            <strong>Datos de suario:</strong>
            <br/>
            <strong>Usuario</strong>: ${form.user}, <strong>Correo:</strong> ${form.email},<strong>Pregunta Secreta:</strong>  ${form.question},<strong>Respuesta secreta:</strong>  ${form.answer}.  <strong>Contraseña:<i>${form.passwordA}</i></strong>.`
    };
    transporter.sendMail(mailOptions, function (err, info) {
        if (err)
            console.log(err);
        else
            console.log(info);
    });
}